FactoryGirl.define do
  factory :venture do
    user
    user_story
    name "A new name"
    description "A new description"
  end
end

require "rails_helper"

RSpec.describe Api::V1::UserStoriesController, type: :controller do
  render_views
  fixtures :users, :ventures, :user_stories

  let(:user) { users(:user_1) }
  let(:auth_headers) { request.headers["Authorization"] = "Token token=#{user.auth_token}" }
  let(:invalid_auth_headers) { request.headers["Authorization"] = "Token token=asqeqkdacmaoskdqo" }
  let(:venture) { ventures(:venture_1) }
  let(:user_story) { user_stories(:user_story_1) }
  let(:user_story2) { user_stories(:user_story_2) }

  describe "GET 'show'" do

    before :each do
      get :show, params: { venture_id: venture.id, id: user_story.id }, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return the correct user story when the correct id is given" do
      body = JSON.parse(response.body)
      expect(body["title"]).to eq("First story title")
      expect(body["content"]).to eq("First story content")
      expect(body["status"]).to eq("in_progress")
    end

    it "Should return 401 on wrong authorization headers" do
      get :show, params: { venture_id: venture.id, id: user_story.id }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "GET 'index'" do

    before :each do
      get :index, params: { venture_id: venture.id }, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return all the user stories from the database for the current user" do
      body = JSON.parse(response.body)
      expect(body["user_stories"][0]["title"]).to eq(user_story.title)
      expect(body["user_stories"][1]["title"]).to eq(user_story2.title)
      expect(body["user_stories"][0]["content"]).to eq(user_story.content)
      expect(body["user_stories"][1]["content"]).to eq(user_story2.content)
      expect(body["user_stories"][0]["status"]).to eq(user_story.status)
      expect(body["user_stories"][1]["status"]).to eq(user_story2.status)
    end

    it "Should return 401 on wrong authorization headers" do
      get :index, params: { venture_id: venture.id }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "POST 'create'" do

    before :each do
      @new_story_params = { title: "first new user_story", content: "first story content", status: "in_progress", venture: venture }
    end

    it "Should increase the number of user stories by one on success" do
      expect { post :create, params: { venture_id: venture.id, user_story: @new_story_params }, headers: auth_headers, format: "application/json" }.to change(UserStory, :count).by(1)
    end

    it "Should return 400 on story name not provided" do
      @new_story_params = { content: "first story content", status: "in_progress", venture: venture }
      post :create, params: { venture_id: venture.id, user_story: @new_story_params }, headers: auth_headers, format: "application/json"
      expect(response).to have_http_status(400)
    end
  end

  describe "PUT 'update'" do
    before :each do
      @updated_story_params = { title: "update first new user_story", content: "update first story content", status: "completed" }
      put :update, params: { id: user_story.id, venture_id: venture.id, user_story: @updated_story_params }, headers: auth_headers, format: "application/json"
    end

    it "Should update already present user story" do
      expect(response).to be_success
    end

    it "Should correctly update the user story" do
      body = JSON.parse(response.body)
      expect(body["content"]).to eq("update first story content")
      expect(body["title"]).to eq("update first new user_story")
      expect(body["status"]).to eq("completed")
    end
  end

  describe "DELETE 'destroy'" do
    before :each do
      delete :destroy, params: { id: user_story.id }, headers: auth_headers, format: "application/json"
    end

    it "Delete should be successful" do
      expect(response).to be_success
    end
    it "Should return all user stories minus the one we just deleted" do
      get :index, params: {}, headers: auth_headers, format: "application/json"
      body = JSON.parse(response.body)
      expect(body["user_stories"].size).to eq 1
    end
  end

end

require "rails_helper"

RSpec.describe Api::V1::CommentsController, type: :controller do
  render_views
  fixtures :users, :ventures, :user_stories, :comments

  let(:user) { users(:user_1)  }
  let(:auth_headers) { request.headers["Authorization"] = "Token token=#{user.auth_token}"  }
  let(:invalid_auth_headers) { request.headers["Authorization"] = "Token token=asqeqkdacmaoskdqo"  }
  let(:venture) { ventures(:venture_1)  }
  let(:user_story) { user_stories(:user_story_1) }
  let(:user_story2) { user_stories(:user_story_2) }
  let(:comment) { comments(:comment_1) }
  let(:comment2) { comments(:comment_2) }

  describe "GET 'index'" do

    before :each do
      get :index, params: { user_story_id: user_story.id }, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return all comments in the database for the user story specified by the id" do
      body = JSON.parse(response.body)
      # binding.pry
      expect(body["comments"][0]["content"]).to eq(comment.content)
    end

    it "Should return 401 on wrong authorization headers" do
      get :index, params: { user_story_id: user_story.id }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
      # expect (response.status).to eq(401)
    end
  end

  describe "GET 'show'" do

      before :each do
        get :show, params: { user_story_id: user_story.id, id: comment.id  }, headers: auth_headers, format: "application/json"
      end

      it "Should be successful" do
        expect(response).to be_success
      end

      it "Should return the correct comment when the correct id is given" do
        body = JSON.parse(response.body)
        expect(body["content"]).to eq(comment.content)
        # binding.pry
      end

      it "Should return 401 on wrong authorization headers" do
          get :show, params: { id: comment.id  }, headers: invalid_auth_headers, format: "application/json"
          expect(response).to have_http_status(:unauthorized)
          # expect (response.status).to eq(401)
        end
    end

  describe "POST 'Create'" do

    before :each do
      @new_comment_params = { content: "new comment", user_story: :user_story_2 }
    end

    it "Should increase the number of comments by one on success" do
      expect { post :create, params: { user_story_id: user_story2.id, comment: @new_comment_params }, headers: auth_headers, format: "application/json" }.to change(Comment, :count).by(1)
    end

    it "Should return 401 on wrong authorization headers" do
      post :create, params: { user_story_id: user_story.id, comment: @new_comment_params }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
      # expect (response.status).to eq(401)
    end
  end

  describe "PUT 'update'" do
    before :each do
      @updated_comment_params = { content: "updated content" }
      put :update, params: { user_story_id: user_story.id, id: comment.id, comment: @updated_comment_params }, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should correctly update the comment's fields" do
      body = JSON.parse(response.body)
      expect(body["content"]).to eq("updated content")
    end
  end

  describe "DELETE 'destroy'" do
    before :each do
      delete :destroy, params: { user_story_id: user_story.id, id: comment.id }, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return 401 on wrong authorization headers" do
      delete :destroy, params: { user_story_id: user_story.id, id: comment.id }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
      # expect (response.status).to eq(401)
    end

    it "Should return all comments minus the one we just deleted" do
      get :index, params: { user_story_id: user_story.id }, headers: auth_headers, format: "application/json"
      body = JSON.parse(response.body)
      expect(body["comments"].size).to eq 0
    end

  end

end

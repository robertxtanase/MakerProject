require "rails_helper"

RSpec.describe Api::V1::VenturesController, type: :controller do
  render_views
  fixtures :users, :ventures, :user_stories

  let(:user) { users(:user_1) }
  let(:user2) { users(:user_2) }
  let(:auth_headers) { request.headers["Authorization"] = "Token token=#{user.auth_token}" }
  let(:invalid_auth_headers) { request.headers["Authorization"] = "Token token=asqeqkdacmaoskdqo" }
  let(:venture) { ventures(:venture_1) }
  let(:venture2) { ventures(:venture_2) }

  describe "GET 'show'" do
    before :each do
      get :show, params: { id: venture.id }, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return the correct venture when the correct id is given" do
      body = JSON.parse(response.body)
      expect(body["name"]).to eq(venture.name)
      expect(body["description"]).to eq(venture.description)
      # binding.pry
      expect(body["user_stories"][0]["title"]).to eq(venture.user_stories[0].title)
      expect(body["user_stories"][1]["title"]).to eq("Second story title")
    end

    it "Should return 401 on wrong authorization headers" do
      get :show, params: { id: venture.id }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
      # expect (response.status).to eq(401)
    end
  end

  describe "GET 'index'" do

    before :each do
      get :index, params: {}, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return all ventures in the database for the current logged user" do
      body = JSON.parse(response.body)
      # binding.pry
      expect(body["ventures"][0]["name"]).to eq(venture.name)
      expect(body["ventures"][1]["name"]).to eq(venture2.name)
      expect(body["ventures"][0]["description"]).to eq(venture.description)
      expect(body["ventures"][1]["description"]).to eq(venture2.description)
    end

    it "Should return 401 on wrong authorization headers" do
      get :index, params: {}, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
      # expect (response.status).to eq(401)
    end
  end

  describe "POST 'Create'" do

    before :each do
      @new_vent_params = { name: "new_vent", description: "new_desc", user: :user_2 }
    end

    it "Should increase the number of ventures by one on success" do
      expect { post :create, params: { venture: @new_vent_params }, headers: auth_headers, format: "application/json" }.to change(Venture, :count).by(1)
    end

    it "Should return 401 on wrong authorization headers" do
      post :create, params: { venture: @new_vent_params }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
      # expect (response.status).to eq(401)
    end
  end

  describe "DELETE 'destroy'" do
    before :each do
      delete :destroy, params: { id: venture.id }, headers: auth_headers, format: "application/json"
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return 401 on wrong authorization headers" do
      delete :destroy, params: { id: venture.id }, headers: invalid_auth_headers, format: "application/json"
      expect(response).to have_http_status(:unauthorized)
      # expect (response.status).to eq(401)
    end

    it "Should return all ventures minus the one we just deleted" do
      get :index, params: {}, headers: auth_headers, format: "application/json"
      body = JSON.parse(response.body)
      expect(body["ventures"].size).to eq 1
    end

  end

end

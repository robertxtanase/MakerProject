set :stage, :staging
set :nginx_server_name, "api.staging.ideatify.pro"
server "api.staging.ideatify.pro", user: fetch(:user) , roles: %w{web app db}, primary: true

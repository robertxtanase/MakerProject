Rails.application.config.middleware.use OmniAuth::Builder do
  # provider :twitter, '4tAWwXnx99wkYF1njeMA9sPv1', 'zuD6gmTj0a5sPHyAZyt5jcnIDYPhEGKGvLUS4y3Od9eAAmaO38'
  secrets = Rails.application.secrets
  provider :twitter, secrets[:twitter_consumer_key], secrets[:twitter_consumer_secret]
end

Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    get "v1/status"

    namespace :v1 do
      post "/session", to: "session#create"

      resources :users, only: [:index, :show, :create, :edit, :destroy]

      resources :ventures, only: [:index, :show, :create, :destroy], shallow: true do
        resources :user_stories, except: [:edit, :new], shallow: true do
          resources :comments,  except: [:edit, :new]
        end
      end

    end
  end

  get "/auth/twitter/callback", to: "api/v1/omniauth_callbacks#twitter"
  get "api/v1/login_done", to: "api/v1/omniauth_callbacks#login_done"
  post "/api/v1/authentications", to: "authentications#create"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

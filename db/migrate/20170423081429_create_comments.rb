class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.text :content
      t.timestamps
    end
    add_reference :comments, :user_story, foreign_key: true
  end
end

class AddAccessSecretToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :access_secret, :string
  end
end

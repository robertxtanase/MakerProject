json.ventures @ventures do |venture|
  json.id venture.id
  json.name venture.name
  json.description venture.description
end

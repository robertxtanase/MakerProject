json.(@user_story, :id, :title, :content, :status)

json.comments @user_story.comments do |comment|
  json.id comment.id
  json.content comment.content
end

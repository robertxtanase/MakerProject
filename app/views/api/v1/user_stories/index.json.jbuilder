json.user_stories @user_stories do |user_story|
  json.id user_story.id
  json.title user_story.title
  json.content user_story.content
  json.status user_story.status
end

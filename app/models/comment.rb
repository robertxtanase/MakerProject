class Comment < ApplicationRecord
  belongs_to :user_story
  has_one :user, through: :user_story
  validates :content, presence: true
end

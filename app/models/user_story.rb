class UserStory < ApplicationRecord
  enum status: [:in_progress, :completed]

  belongs_to :venture
  has_one :user, through: :venture
  has_many :comments, dependent: :destroy
  validates :title, presence: true

  before_save :assign_status

  private

    def assign_status
      self.in_progress! unless status.present?
    end
end

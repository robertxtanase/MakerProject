class Api::V1::CommentsController < ApplicationController
  before_action :require_authorization

  def index
    @comments = Comment.where(user_story_id: params[:user_story_id])
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def create
    @comment = Comment.new(comments_params);
    @comment.user_story = UserStory.find(params[:user_story_id])

    if @comment.save
      render :show
    else
      render_errors(message: "Something went wrong - Paramater missing.")
    end
  end

  def update
    @comment = Comment.find(params[:id])
    head 404 unless @comment

    @comment.update(comments_params)

    render :show

    rescue ActiveRecord::ValidationFailed
      render_errors(message: @comment.errors, status: 400)
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    head 200
  end

  private

    def comments_params
      params.require(:comment).permit(:content)
    end
end

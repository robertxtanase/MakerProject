class Api::V1::VenturesController < ApplicationController
  before_action :require_authorization

  def index
    @ventures = Venture.where(user_id: current_user.id)
    # binding.pry
  end

  def show
    @venture = Venture.find(params[:id])
  end

  def create
    @venture = Venture.new(venture_params)
    @venture.user = current_user

    if @venture.save
      render :show
    else
      render_errors(message: "Something went wrong - Paramater missing.")
    end
  end

  def destroy
    @venture = Venture.find(params[:id])
    @venture.destroy

    head 200
    # head :ok
  end

  private

    def venture_params
      params.require(:venture).permit(:name, :description)
    end
end

class Api::V1::UserStoriesController < ApplicationController
  before_action :require_authorization

  def index
    @user_stories = UserStory.where(venture_id: params[:venture_id])
  end

  def show
    @user_story = UserStory.find(params[:id])
  end

  def create
    @user_story = UserStory.new(user_stories_params)
    @user_story.venture = Venture.find(params[:venture_id])

    if @user_story.save
      render :show
    else
      render_errors(message: "Something went wrong - Paramater missing.")
    end
  end

  def update
    @user_story = UserStory.find(params[:id])
    head 404 unless @user_story

    @user_story.update(user_stories_params)

    render :show

    rescue ActiveRecord::ValidationFailed
      render_errors(message: @user_story.errors, status: 400)
  end

  def destroy
    @user_story = UserStory.find(params[:id])
    @user_story.destroy

    head 200
  end

  private

    def user_stories_params
      params.require(:user_story).permit(:title, :content, :status)
    rescue ActionController::ParameterMissing
      head 400
    end
end
